const { names } = require("../src/util.js");

module.exports = (app) => {
  app.get("/", (request, response) => {
    return response.send({ data: {} });
  });

  app.get("/people", (request, response) => {
    return response.send({
      people: names,
    });
  });

  app.post("/person", (request, response) => {
    //if no NAME key
    if (!request.body.hasOwnProperty("name")) {
      return response.status(400).send({
        error: "Bad Request - missing require parameter NAME",
      });
    }
    //if NAME is not string
    if (typeof request.body.name !== "string") {
      return reposnse.status(400).send({
        error: "Bad Request - NAME has to be a string",
      });
    }

    //if no AGE key
    if (!request.body.hasOwnProperty("age")) {
      return response.status(400).send({
        error: "Bad Request - missing require parameter AGE",
      });
    }

    //if AGE is not a number
    if (typeof request.body.age !== "number") {
      return reposnse.status(400).send({
        error: "Bad Request - Age has to be a number",
      });
    }

    //ACTIVITY
    if (!request.body.hasOwnProperty("alias")) {
      return response.status(400).send({
        error: "Bad Request - missing require parameter alias",
      });
    }
  });
};
