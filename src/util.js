function factorial(n) {
  if (typeof n !== "number") return undefined;
  if (n < 0) return undefined;
  if (n === 0) return 1;
  if (n === 1) return 1;
  return n * factorial(n - 1);
}

function oddEven(num) {
  if (typeof num !== "number") return undefined;
  if (num % 2 === 0) {
    return "Even";
  } else {
    return "Odd";
  }
}

function div_check(num) {
  if (typeof num !== "number") return undefined;
  //105 is divisible by 5 and 7. For now set is to 5
  if (num === 105 || (num % 5 === 0 && num % 7 !== 0)) {
    return "Divisible by 5";
  } else if (num % 5 !== 0 && num % 7 === 0) {
    return "Divisible by 7";
  } else if (num % 5 === 0 || num % 7 === 0) {
    return "Divisible by 5 or 7";
  } else {
    return "Not Divisible by 5 or 7";
  }
}

const names = {
  Boba: {
    name: "Boba Fett",
    age: 50,
  },
  Anakin: {
    name: "Anakin Skywalker",
    age: 65,
  }
};



module.exports = {
  factorial: factorial,
  oddEven: oddEven,
  div_check: div_check,
  names
};
