const chai = require('chai')
const expect = chai.expect;
//same as :
//const {expect} = require('chai)

const http = require('chai-http')

//since gagamitin si chai http module in our in our chai-http 

chai.use(http);

describe("API Test Suite", () => {
    it("Test API GET People are running", () => {
        chai.request('http:localhost:5001').get('/people')
        .end((error,response) => {
            expect(response).to.not.equal(undefined)
        })
    })

    it("Test API GET people returns 200", (done) => {
		chai.request('http://localhost:5001')
		.get('/people')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();
		})
	})


    it("Test API POST Person return 400 if no person name", (done) => {
        chai.request('http:localhost:5001')
        .post('/person')
        .type('json')
        .send({
            alias:"Jason",
            age:28
        })
        .end((error,response) => {
            expect(response.status).to.equal(400);
            done();
        })
    })

    it("[1] Check if the POST endpoint is running", (done) => {
        chai.request('http:localhost:5001')
        .post('/people')
        .type('json')
        .send({
            name:"Jason",
            alias:"Jason",
            age:28
        })
        .end((error,response) => {
            expect(response).to.not.equal(undefined)
            done();
        })
    })

    it("[2] Check if the POST endpoint encounters an error if there is no alias", (done) => {
        chai.request('http:localhost:5001')
        .post('/person')
        .type('json')
        .send({
            name:"Jason",
            age:28
        })
        .end((error,response) => {
            expect(response.status).to.equal(400);
            done();
        })
    })

    it("[3]  Check if the POST endpoint encounters an error if there is no age", (done) => {
        chai.request('http:localhost:5001')
        .post('/person')
        .type('json')
        .send({
            name:"Jason",
            alias:"Jason"
        })
        .end((error,response) => {
            expect(response.status).to.equal(400);
            done();
        })
    })
})