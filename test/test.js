const { factorial, oddEven, div_check } = require('../src/util.js')
const { expect, assert } = require('chai');



describe('test fun factorials', () => {
    it('test_fun_factorial_5!_is_120', () => {
        const product = factorial(5);
        expect(product).to.equal(120);
    })
    
    it('test_fun_factorial_1!_is_1', () => {
        assert.equal(factorial(1),1)
    })
})

describe('test_num', () => {
    it('test_2_is Even',() => {
        assert.equal(oddEven(2),'Even')
    })

    it('test_5_is_Odd',() => {
        assert.equal(oddEven(5),'Odd')
    })
})

describe('Activity', () => {
    it('0!',() => {
        assert.equal(factorial(0),1)
    })

    it('4!',() => {
        assert.equal(factorial(4),24) 
    })

    it('10!',() => {
        expect(factorial(10)).to.equal(3628800)
    })

    it('105 should be divisible by 5', () => {
        assert.equal(div_check(105),'Divisible by 5')
    })

    it('14 should be divisible by 7', () => {
        assert.equal(div_check(14),'Divisible by 7')
    })

    it('0 should be divisible by 5 or 7', () => {
        assert.equal(div_check(0),'Divisible by 5 or 7')
    })

    it('22 should not be divisible by 5 or 7', () => {
        assert.equal(div_check(22),'Not Divisible by 5 or 7')
    })

})

describe('thursday', () => {
    it('test factorial -1 is undefined',() => {
        // const product = -1
        const product = factorial(-1)
        expect(product).to.equal(undefined)
    })

    it('return error if input is non numeric', () => {
        const product = factorial(5)
        expect(product).to.equal(undefined)
    })
})

describe('thursday activity', ()=>{
    it('factorial: return error if input is non numeric', () => {
        const product = factorial('a')
        expect(product).to.equal(undefined)
    })

    it('oddEven: return error if input is non numeric', () => {
        const product = oddEven('a')
        expect(product).to.equal(undefined)
    })

    it('div_check: return error if input is non numeric', () => {
        const product = div_check('a')
        expect(product).to.equal(undefined)
    })

})